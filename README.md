========
tp9605bt
========

Overview
========
This distro provides some notes and software for the Tekpower TP9605BT 3 3/4 digit digital multimeter. An API written by Jim Leonard for the TP4000ZC served as inspiration, although that meter had radically different logging data format.

The TP9605BT is an inexpensive DMM that has an optically isolated USB interface and bluetooth logging to .csv file features. The Python code here is strictly limited to the USB logging feature and has nothing to do with the bluetooth feature.

This code is PRE-ALPHA! It is not fully debugged, let alone feature complete. Caveat Programmer.

Documentation
=============

Thanks to GitHub user tlbdk for having the protocol doc in his repo that helped me find the PDF on the tekpower site. No need to complete a reverse engineering job!

Contents
========
Python command script "tp9605cat" 'cats' meter logging output to standard output at the same rate as received by default, displaying the Unix epoch elapsed hundredth seconds, ISO date/time, magnitude and unit. For example here is output when the meter is set to measure degrees C:
* 147371280875 Mon Sep 12 16:40:08 2016 29.0 C
* 147371280971 Mon Sep 12 16:40:09 2016 29.1 C
* 147371281067 Mon Sep 12 16:40:10 2016 29.1 C
Use "tp9605cat -h" for a complete list of options

API "tp9605bt.py" offers a simple class with methods for connecting to and accessing meter data. The tp9605cat program shows some API usage.

TODO: Leveraging the Tekpower protocol will allow getting the API class sorted and allow the cat program to just use the API. 

Installation
============
(Incomplete!)
Copy the tp9605bt.py file into your current directory and use "import tp9605bt" to use the API. Use "make" in the documentation directory to build html docs in a _build subdirectory. Use "make help" for other doc build options. This depends on Sphinx. API docs are planned and a proper setup.py is also planned.
 
