TP9605BT DMM Custom /dev/tp9605bt device for Linux
==================================================

To avoid confusion with just which device called /dev/ttyUSB* is actually connected to the DMM, add this in a uniquely named file (e.g. "55-tekpower-tp9605bt") in /etc/udev/udev.rules

SUBSYSTEM=="tty", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", ATTRS{serial}=="0000:00:04.1", SYMLINK+="tp9605bt"

