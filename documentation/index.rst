.. tp9605bt documentation master file, created by
   sphinx-quickstart on Sat Sep  3 11:04:54 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tp9605bt's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   meter-notes
   using-udev


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

