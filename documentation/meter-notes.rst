TP9605BT DMM Notes
==================

Here are some informal notes about the Tekpower TP9605BT DMM. A few details are not fully explored, but it should overall do more good than harm.

High Order Bits
---------------

- The TP9605BT generates USB logging data that is mostly reliable and highly useful once the data format is decoded. The availability of logging data via bluetooth connections to a smart phone is another plus of this meter (but does not relate to the Python code here). It is a competant 3 3/4 digit meter with many nice features, such as automatic measurement of AC and DC voltage and current with a single switch setting. It comes with a temperature probe and optically isolated USB interface.
- This meter eats batteries. The typical current draw is nearly 50mA (25mA after a bluetooth connection made), translating to just a few hours for a typical nine volt battery. This meter's auto-shutoff doesn't work, so although the screen blanks and logging output stops, the 50/25mA draw continues unabated. This makes the already short life of a nine volt battery a joke if a user falls back on old habits and forgets to turn the meter to one of the "off" switch positions.

Addenda to the User Docs
------------------------

The meter has a lot of characteristics that are not covered in the `TP9605BT User Manual`_ and deserve documentation. Here is a list in no particular order.

1) Current draw is 48-49 milliamperes during normal operation up to the point a bluetooth connection with something like a smart phone is established, at which point it drops to about 25 milliamperes. The draw goes up 3-4 mA when the back light is turned on and about 30 mA when the continuity tester is beeping.
2) As mentioned above, the auto-shutoff doesn't actually disconnect the meter from the battery. The rotary switch must be in the left or right "off" position to stop current draw properly.
3) The auto-shutoff defeat described in the Tekpower user manual is mistaken. They instruct users to press and hold the "select" button before moving away from "off", but the correct button to press and hold is the "range" button.
4) When the "rs232/delta" button is pressed and held to initiate USB output (that has nothing to do with rs232, by the way), the "delta" measurement mode is always turned on too when in many modes, making it necessary to press the button one more time to turn the delta mode back off. The fact that this behavior doesn't happen for some modes, such as temperature, makes it a major annoyance to remember which sequence to use to avoid fouling up the logging and/or delta states.
5) USB output does not happen when the meter is set to millivolts at the time power is turned on (and rs232 is pressed and held) and the meter cannot display the input voltages (for example, .9 volts, which is out of range for the "millivolt" setting). The various modes haven't been tested to see if others interfere with USB output if an "overrange" condition is present.
6) USB output doesn't happen at all for frequency measurements
7) The auto-calibrate mode is not initiated if there is a small resistance across the leads, or at least that's the current superstition. More tests needed for this.
8) Auto-calibration didn't change the behavior of the meter when reading temperature and the accuracy didn't seem good for initial tests. More tests are needed.
9) The current draw of the meter is the same (within a milliampere) with or without USB (again, labeld "rs23") logging enabled, making one wonder why it needs to be turned on.
10) Quick tests suggest the USB sample output rate is 172.8 samples per minute for voltage measurements and 58 per minute for temperature.
11) One of the two probes received with the meter was defective, having an intermittent connection giving wacky resistance measurements as varying amounts of pressure were applied to the probe tip. These went to the trash can two days after the meter was received. Decent probes are so inexpensive this flaw is deemed noise, and searching through the Amazon site selling the meter with hundreds of reviews (although some obviously for a different meter) shows nobody railing about the probes. So this goes into the "for what it's worth" department.
12) The power supply voltage requirement is very much lower than one might think. The meter will run reliably, including bluetooth connection, at a hair over three volts while keeping it's "battery low" indicator off. The meter flakes out at about 2.2 volts and the bluetooth somewhere between there and three. Current stays close ot constant across this range.
13) When the meter is in "no auto shutoff mode" it beeps every few minutes. This keeps a person from getting too lost in thought. 


.. _`TP9605BT User Manual`: http://www.kaitousa.com/index.php/miscellaneous/tp9605bt.html
.. _`TP9605BT Decoder Ring`: http://tekpower.us/downloadable/download/sample/sample_id/31/
